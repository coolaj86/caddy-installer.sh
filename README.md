Caddy Installer
---------------

Works on

  * Ubuntu Linux
  * macOS Sierra
  * probably lots of others

Supports

  * **systemd** (Ubuntu, Fedora, Arch, etc)
  * **launchd** (OS X, macOS, Darwin)

Quick 'n' Dirty
------

**Caddy**:

```bash
curl -L https://git.coolaj86.com/coolaj86/caddy-installer.sh/raw/master/install-caddy | bash
```

**Caddy + Features**:

```bash
curl -L https://git.coolaj86.com/coolaj86/caddy-installer.sh/raw/master/install-caddy | bash -s -- search,realip
```

The default site location is `/srv/www/` and the server begins immediately.

Test it at

  * <http://localhost>

Install... the installer
-------

Yes... you install the installer

```bash
# download
curl -L https://git.coolaj86.com/coolaj86/caddy-installer.sh/raw/master/install-caddy -o install-caddy

#change permissions
sudo chown root:root ./install-caddy
sudo chmod a+x ./install-caddy

# move
sudo mv ./install-caddy /usr/local/bin/install-caddy
```

Usage
-----

```
install-caddy --help

Usage: install-caddy [plugin1,plugin2,...]

Optional arguments (defaults shown)
  --os darwin      # any of windows darwin linux freebsd openbsd
  --arch amd64     # any of arm64 amd64 386 armv5 armv6l armv7l

Features:
DNS,awslambda,cors,expires,filemanager,filter,git,hugo,ipfilter,jsonp,jwt,locale,mailout,minify,multipass,prometheus,ratelimit,realip,search,upload,cloudflare,digitalocean,dnsimple,dyn,gandi,googlecloud,linode,namecheap,ovh,rfc2136,route53,vultr
```

Where do my websites go?
-------------------

The default site is `/srv/www/localhost`.

If you own a domain (say example.com) then you
should put a new folder in `/srv/www` (such as `/srv/www/example.com`)
and then edit `/etc/caddy/Caddyfile` to include a config such as this:

```Caddyfile
https://example.com {
  root /srv/www/example.com
}
https://www.example.com {
  redir https://example.com
}
```

Start caddy on boot
-------------------

Startup Scripts are installed by the installer :)

  * **systemd** (Ubuntu, Fedora, Arch, etc)
  * **launchd** (OS X, macOS, Darwin)

Caddy should begin running on ports 80 and 443 as soon as you install it
and should automatically start on boot

Caveats
-------

### all platforms

`go` (in which `caddy` is written) doesn't support privilege deescalation
(running on root to bind to port 80 and 443 and then switching to a non-root user).

This isn't usually a problem, however, because the launchers (systemd et al) usually do.

### darwin / macOS / OS X

* `launchd` doesn't support privilege deescalation
* `authbind` doesn't work on recent versions of OS X

However, you can use [ipfw](https://apple.stackexchange.com/questions/37418/how-can-i-open-port-80-so-a-non-root-process-can-bind-to-it) to locally port-forward.

Also, `launchd` is a pain to configure.
There's not much in the way of official documuntation...
but there is some [great unofficial documentation](http://www.launchd.info/)
and [LaunchControl](http://www.soma-zone.com/LaunchControl/)
([direct download](http://www.soma-zone.com/download/files/LaunchControl_1.30.1.tar.bz2))
makes it actually quite easy.
(ignore that the site looks like a 90s spam site - much like MakeMKV - it's actually legit)

Debugging systemd
-----------------

```bash
sudo -u www-data CADDYPATH=/etc/ssl/caddy /usr/local/bin/caddy -log stdout -agree=true -conf=/etc/caddy/Caddyfile -root=/var/tmp
```
